<?php needLogin(); ?>
<?php
    $msg = "Modulo AD desativado, por favor, adicone um Ad como um conteúdo.";
    // irPara(URLADM . "?p=listar_conteudo&msg=$msg"); 
?>
<?php
$get = (isset($_GET['ad_id'])) ? (string)$_GET['ad_id'] : '1';
$where = "where ad_id='{$get}'";
$sql = "select * from adboxes_ads " . $where;
$consulta = $conn->query($sql);
// $haLinhas = $conn->query("select count(*) from adboxes_ads {$where}")->fetchColumn();
$haLinhas = $conn->query($sql)->fetchColumn();
if (!$haLinhas) {
    echo "Registro \"$get\" não encontrado";
} else {
    foreach ($consulta as $keyAd => $valueAd) {
        $ativo = $valueAd['ad_ativo'];
        $conteudo = $valueAd['ad_conteudo']; //htmlspecialchars_decode($valueAd['ad_conteudo']);

        ?>
        <div class="row">
            <div class="col-12">
                <h2>Editar Ad</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="top_box">
                    <strong style="font-size: 0.8em;">Código Ad</strong>
                    <form action="core/Ad_Update.php">
                        <input type="hidden" name="ad_id" value="<?= $get; ?>">
                        <input type="hidden" name="ad_altera_estado">
                        <label><input type="radio" name="ad_ativo" value="sim" <?php if ($ativo == 'sim') {
                                                                                    echo 'checked';
                                                                                } ?> required>Habilitado</label>
                        <label><input type="radio" name="ad_ativo" value="nao" <?php if ($ativo == 'nao') {
                                                                                    echo 'checked';
                                                                                } ?> required>Desabilitado</label>
                        <input type="hidden" name="irPara" value="<?= $estaUrl; ?>">
                        <button class="botao bg-verde" type="submit">Alterar Estado</button>
                        <br>
                        <?php
                        if ($ativo == 'sim') {
                            echo '<strong style="font-size: 0.8em;">Estado: <span class="cor-verde">Habilitado</span> </strong>';
                        } else {
                            echo '<strong style="font-size: 0.8em;">Estado: <span class="cor-vermelho">Desabilitado</span> </strong>';
                        }
                        ?>

                    </form>
                </div>
                <form action="core/Ad_Update.php" method="POST">
                    <input type="hidden" name="ad_id" value="<?= $get; ?>">
                    <div class="col-10">
                        <label> Nome Interno do Ad (Não visível para o público)<br>
                            <input type="text" name="ad_nome" value="<?= $valueAd['ad_nome']; ?>" required>
                        </label>
                    </div>
                    <div class="col-10">
                        <label> Título do Ad (Visível para o público)<br>
                            <input type="text" name="ad_titulo" value="<?= $valueAd['ad_titulo']; ?>" required>
                        </label>
                    </div>
                    <div class="col-10">
                        <strong>Conteúdo:</strong><br>
                        <?php botaoEditorVisual() ?>
                        <textarea class="col-12" id="editor" name="ad_conteudo" cols="30" rows="10"><?= $conteudo; ?></textarea>
                    </div>
                    <div class="col-10">
                        <input type="hidden" name="irPara" value="<?= $estaUrl; ?>">
                    </div>
                    <div class="bottom_box">
                        <input class="botao bg-verde" type="submit" name="ad_atualiza_dados" value="Atualizar">
                    </div>
                </form>
            </div>

        </div>
    <?php
}
}
?>