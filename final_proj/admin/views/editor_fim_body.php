<script>
$('#editor')
.trumbowyg({
    plugins: {
        resizimg : {
        minSize: 64,
        step: 16,
        },
        templates: [
            {
                name: 'Template 1',
                html: '<p>Primeiro template, o Template 1</p>'
            },
            {
                name: 'Template 2',
                html: '<p>Template 2, diferente do Template 1</p>'
            }
        ]
    },
    btnsDef: {
        // Create a new dropdown
        image: {
            dropdown: ['insertImage', 'noembed'],
            ico: 'insertImage'
        }
    },
    // Redefine the button pane
    btns: [
        ['viewHTML'],
        ['formatting'],
        ['strong', 'em', 'del'],
        ['superscript', 'subscript'],
        ['link'],
        ['image'], // Our fresh created dropdown
        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
        ['unorderedList', 'orderedList'],
        ['horizontalRule'],
        ['removeformat'],
        ['foreColor', 'backColor'],
        ['fullscreen'],
        ['template'],
        ['highlight']
    ]
});
</script>