<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Social share meta tags -->
  <meta property="og:title" content="Float Banner">
  <meta property="og:description" content="Float Banner">
  <meta property="og:type" content="website">
  <meta property="og:image" content="http://tiagofranca.com/img/metaThumbImage.png">
  <meta property="og:site_name" content="Float Banner">
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:title" content="Float Banner">
  <meta name="twitter:description" content="Float Banner">
  <meta name="twitter:image:src" content="http://tiagofranca.com/img/metaThumbImage.png">
  <!-- End social share meta tags -->
  <link rel="shortcut icon" href="../assets/img/favicon.png">
  <link rel="stylesheet" href="../assets/css/reset.css">
  <link rel="stylesheet" href="../assets/css/simple-grid.css">
  <title>Float Banner | Gerenciador</title>
  <meta name="description" content="Gerenciador de Banner flutuante">
</head>
<body>
<div class="container">
    <div class="row">
    <div class="col-3">
        This content will take up 3/12 (or 1/4) of the container
    </div>
    <div class="col-3">
        This content will take up 3/12 (or 1/4) of the container
    </div>
    <div class="col-6">
        This content will take up 6/12 (or 1/2) of the container
    </div>
    </div>
</div>
</body>
<footer>
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="img img-logo"></div>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          
        </div>
      </div>
    </div>
  </footer>
</body>
</html>