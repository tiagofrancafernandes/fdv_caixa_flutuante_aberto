<?php
    require_once("../bootstrap.php");
    $request = (isset($_POST))? $_POST:null;
    $qtd = count($request);

    $okContinue = false;
    // $okContinue = true;

if(isset($_POST['acao']) AND $_POST['acao']=="editar"){
    
    if ($request != null or $qtd > 0) {
        if($okContinue != true){
            // redirHeader(URLADM."?p=listar_conteudo#falta-dados");
        }
        $request = (object) $request;
    }else{
        // redirHeader(URLADM."?p=listar_conteudo#falta-dados");
        irPara();
        exit();
    }

    try {

        if(isset($_FILES['cont_img']) AND $_FILES['cont_img']['size'] > 0){

            $errors= array();
            $file_name = $_FILES['cont_img']['name'];
            $file_size = $_FILES['cont_img']['size'];
            $file_tmp = $_FILES['cont_img']['tmp_name'];
            $file_type = $_FILES['cont_img']['type'];
            $file_ext=pathinfo($_FILES['cont_img']['name'], PATHINFO_EXTENSION);
            $expensions= IMG_EXT_ALLOW;
            $toSqlName = "{{URL_IMAGENS_UP}}".$file_name;
            $finalName= $toSqlName;
            $folderUploadCheck = "../../".IMG_FOLDER_UP;
            
            if (!is_writable($folderUploadCheck)) {
                $msg = "Pasta '$folderUploadCheck' sem permissão de escrita";
                irPara(URLADM."?p=listar_conteudo&msg=".$msg);
                $errors[] = $msg;
                exit();
            }
            
            if(in_array($file_ext,$expensions)=== false){
                $msg = "Extensão não permitida.";
                irPara(URLADM."?p=listar_conteudo&msg=".$msg);
                $errors[] = $msg;
                exit();
            }
            
            if($file_size > toMb(IMG_MAX_SIZE)) {
                $msg = "O tamanho do arquivo excede ".IMG_MAX_SIZE." MB";
                irPara(URLADM."?p=listar_conteudo&msg=".$msg);
                $errors[] = $msg;
                exit();
            }
            
            if(empty($errors)==true) {
               move_uploaded_file($file_tmp,$folderUploadCheck.$file_name);
            }/***/
            else{
                $msg = "Erros no upload.";
                irPara(URLADM."?p=listar_conteudo&msg=".$msg);
                $errors[] = $msg;
                exit();
            } /**/
         }else{
            $toSqlName = (isset($_POST['cont_img']) AND count($_POST['cont_img']) > 0)?$_POST['cont_img']:'VAZIO';
         }
         $cont_id = htmlspecialchars($request->cont_id);
         $cont_nome = htmlspecialchars($request->cont_nome);
         $cont_caixa_nome = htmlspecialchars($request->cont_caixa_nome);
         $cont_titulo = htmlspecialchars($request->cont_titulo);
         $cont_link = htmlspecialchars($request->cont_link);
         $cont_img = $toSqlName;
         $imgToSqlName = ($toSqlName != "VAZIO")?"\"cont_img\"='$cont_img'":"\"cont_img\"=NULL";
         $cont_conteudo = htmlspecialchars($request->cont_conteudo);
         $cont_principal = ($_POST['cont_principal'] == 'sim' OR $_POST['cont_principal'] == 'nao') ? $_POST['cont_principal'] :'nao';
         $cont_estado = ($_POST['cont_estado'] == 'habilitado' OR $_POST['cont_estado'] == 'desabilitado') ? $_POST['cont_estado'] :'habilitado';
         $cont_lado = ($_POST['cont_lado'] == 'esquerdo' OR $_POST['cont_lado'] == 'direito') ? $_POST['cont_lado'] :'';
        //  print_r($imgToSqlName);exit();
        
    $sql = "
    UPDATE adboxes_conts SET `cont_id`= $cont_id,
     `cont_nome`='{$cont_nome}',
     `cont_caixa_nome`='{$cont_caixa_nome}',
     `cont_titulo`='{$cont_titulo}',
     {$imgToSqlName},
     `cont_link`='{$cont_link}',
     `cont_conteudo`='{$cont_conteudo}',
     `cont_estado`='{$cont_estado}',
     `cont_principal`='{$cont_principal}',
     `cont_lado`='{$cont_lado}',
     `tipobox`='conteudo' WHERE `cont_id`=$cont_id
     ";

    if($conn->exec($sql)){
        var_dump($sql);
        $msg = "Banner '$cont_id' Atualizado com sucesso!";
        irPara(URLADM."?p=listar_conteudo&msg=".$msg);
        exit();
    }else{
        $msg = "Erro ao atualizar";
        // var_dump($sql);
        exit();
        irPara(URLADM."?p=listar_conteudo&msg=".$msg);
    }

    } catch (\Throwable $th) {
        throw $th;
    }

}elseif(isset($_POST['acao']) AND $_POST['acao']=="adicionar"){ //Até aqui se estiver editando, senao, daqui pra baixo        
    
    if ($request != null or $qtd > 0) {
        if($okContinue != true){
            // redirHeader(URLADM."?p=listar_conteudo#falta-dados");
        }
        $request = (object) $request;
    }else{
        // redirHeader(URLADM."?p=listar_conteudo#falta-dados");
        irPara();
        exit();
    }

        try {
            $cont_nome = htmlspecialchars($request->cont_nome);
            $cont_caixa_nome = htmlspecialchars($request->cont_caixa_nome);
            $cont_titulo = htmlspecialchars($request->cont_titulo);
            $cont_link = htmlspecialchars($request->cont_link);
            $cont_img = htmlspecialchars($request->cont_img);
            $cont_conteudo = htmlspecialchars($request->cont_conteudo);
            $cont_estado = ($_POST['cont_estado'] == 'habilitado' OR $_POST['cont_estado'] == 'desabilitado') ? $_POST['cont_estado'] :'habilitado';
        $sql = "
            INSERT INTO 'adboxes_conts'
            (
                'cont_id','cont_nome','cont_caixa_nome','cont_titulo','cont_img','cont_link','cont_conteudo','cont_estado','cont_lado'
            ) 
            VALUES
            (
                NULL,'{$cont_nome}','{$cont_caixa_nome}','{$cont_titulo}','{$cont_img}','{$cont_link}','{$cont_conteudo}','{$cont_estado}',''
            )
        ";
        if($conn->exec($sql)){
            $msg = "Adicionado com sucesso!";
            irPara(URLADM."?p=listar_conteudo&msg=".$msg);
            exit();
            // $msg = "Erro ao adicionar";
        }else{
            irPara(URLADM."?p=listar_conteudo&msg=".$msg);
            exit();
        }

        } catch (\Throwable $th) {
            throw $th;
        }
    } else {
		$msg = "Ação indevida!";
		irPara(URLADM . "?p=listar_conteudo&msg=$msg");
	}
?>