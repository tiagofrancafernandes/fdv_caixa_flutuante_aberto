<?php
require_once './admin/bootstrap.php';

ini_set("allow_url_fopen", 1);
$urlEsquerda = URLADM . 'api.php?lado=esquerdo';
$urlDireita = URLADM . 'api.php?lado=direito';

$jsonDir = file_get_contents($urlDireita);
$jsonDireita = json_decode($jsonDir);
$contaDireita = count($jsonDireita);

$jsonEsq = file_get_contents($urlEsquerda);
$jsonEsquerda = json_decode($jsonEsq);
$contaEsquerda = count($jsonEsquerda);

//Caixas habilitadas ou não
$estadoEsquerda = singleResult('caixa_estado', 'adboxes_boxes', "`caixa_nome`='esquerda'");
$estadoDireita = singleResult('caixa_estado', 'adboxes_boxes', "`caixa_nome`='direita'");
$caixaEsquerda  = ($estadoEsquerda == "on") ? true : false;
$caixaDireita   = ($estadoDireita == "on") ? true : false;


if ($contaEsquerda > 0) {

    $jsonEsquerda = $jsonEsquerda[0];
    $esquerdaTipo;
    $esquerdaId;
    $esquerdaTitulo;
    $esquerdaContCaixaNome;
    $esquerdaLink;
    $esquerdaConteudo;
    $esquerdaImg;
    $esquerdaTipo = $jsonEsquerda->tipobox;

    switch ($esquerdaTipo) {
        case 'ads':
            $esquerdaId = $jsonEsquerda->ad_id;
            $esquerdaTitulo = $jsonEsquerda->ad_titulo;
            $esquerdaContCaixaNome = "";
            $esquerdaLink = $jsonEsquerda->ad_link;
            $esquerdaConteudo = $jsonEsquerda->ad_conteudo;
            break;

        case 'conteudo':
            $esquerdaId = $jsonEsquerda->cont_id;
            $esquerdaTitulo = $jsonEsquerda->cont_titulo;
            $esquerdaContCaixaNome = $jsonEsquerda->cont_caixa_nome;
            $esquerdaLink = $jsonEsquerda->cont_link;
            $esquerdaConteudo = $jsonEsquerda->cont_conteudo;
            $esquerdaImg = $jsonEsquerda->cont_img;
            $haEsquerdaImg = (strlen((string)$esquerdaImg) > 5)?true:false;
            $esquerdaImg = str_replace("{{URL_IMAGENS_UP}}", IMG_URL_FULL, $esquerdaImg);
            break;

        default:
            exit();
            break;
    }
}

if ($contaDireita > 0) {

    $jsonDireita = $jsonDireita[0];
    $direitaTipo;
    $direitaId;
    $direitaTitulo;
    $direitaContCaixaNome;
    $direitaLink;
    $direitaConteudo;
    $direitaImg;
    $direitaTipo = $jsonDireita->tipobox;

    switch ($direitaTipo) {
        case 'ads':
            $direitaId = $jsonDireita->ad_id;
            $direitaTitulo = $jsonDireita->ad_titulo;
            $direitaContCaixaNome = "";
            $direitaLink = $jsonDireita->ad_link;
            $direitaConteudo = $jsonDireita->ad_conteudo;
            break;

        case 'conteudo':
            $direitaId = $jsonDireita->cont_id;
            $direitaTitulo = $jsonDireita->cont_titulo;
            $direitaContCaixaNome = $jsonDireita->cont_caixa_nome;
            $direitaLink = $jsonDireita->cont_link;
            $direitaConteudo = $jsonDireita->cont_conteudo;
            $direitaImg = $jsonDireita->cont_img;
            $haDireitaImg = (strlen((string)$direitaImg) > 5)?true:false;
            $direitaImg = str_replace("{{URL_IMAGENS_UP}}", IMG_URL_FULL, $direitaImg);
            break;

        default:
            exit();
            break;
    }
}

?>


<?php if ($caixaEsquerda || $caixaDireita) { ?>
    <?php require_once 'estilo.php'; ?>
    <div class="floating tgo-fixed float_pai mostra_oculta_logado">

        <?php if ($caixaEsquerda) { ?>
            
            <?php if ($contaEsquerda > 0) { ?>

            <?php $estadoCookieEsquerda = (isset($_COOKIE['estadoCaixaEsquerda']))?$_COOKIE['estadoCaixaEsquerda']:'mostrar'; ?>
            <?php if ($estadoCookieEsquerda != 'ocultar') { ?>

                <div class="animacao-in mostra_oculta_logado tgo-fixed tgo-xs-left0 tgo-backgroundColorWhite tgo-borderBox tgo-borderLighter tgo-borderRadius4 tgo-xs-borderRadius0 tgo-topPercent tgo-left20 tgo-xs-initialPosition tgo-xs-bottom0 tgo-xs-maxSizeFullWidth tgo-xs-maxSizeFullHeight tgo-textAlignLeft uiScale maxw">

                    <div class="tgo-absolute box_cabecalho" style="left:0;">
                        <span class='classe_fechar' onclick='fecharEsquerda(this)'>x</span>
                    </div>
                    <div style="border-bottom: #757373 1px solid;background: #80808026;">
                        <span class="tipo_box_interna"><?php echo $esquerdaContCaixaNome; ?></span>
                        <br style="width: 100%;">
                    </div>
                    <div class="box_interna">

                        <?php if ($esquerdaTipo == 'conteudo') { ?>
                            <div>
                                <a href="<?php echo $esquerdaLink; ?>">
                                    <span class="float_titulo"><?php echo $esquerdaTitulo; ?></span>
                                </a>
                            </div>
                            <?php if($haEsquerdaImg){ ?>
                            <a href="<?php echo $esquerdaLink; ?>">
                                <img src="<?php echo $esquerdaImg; ?>" class="img_banner_float" alt="" />
                            </a>
                            <?php } ?>
                            
                            <div class="ui-caption">
                                <?php //limitChars($esquerdaConteudo, 250); ?>
                                <?php echo htmlspecialchars_decode($esquerdaConteudo); ?>
                            </div>
                            <div class="ui-caption">
                                <!-- <a class="ds-link ds-link--styleSubtle" href="<?php echo $esquerdaLink; ?>" target="_blank">Ver mais &gt;&gt;</a> -->
                            </div>
                        <?php } elseif ($esquerdaTipo == 'ads') { ?>
                            <div class="ui-caption">
                                <?php echo htmlspecialchars_decode($esquerdaConteudo); ?>
                            </div>
                        <?php } else {
                        ?>
                            <div class="ui-caption">
                                <?php echo htmlspecialchars_decode($esquerdaConteudo); ?>
                            </div>
                        <?php
                    } ?>

                    </div>
                </div>
                <!-- Fim do box esquerda -->

            <?php } //Verifica cookie esquerda ?>

            <?php } ?>

        <?php } ?>

        <?php if ($caixaDireita) { ?>
            <?php if ($contaDireita > 0) { ?>

            <?php $estadoCookieDireita = (isset($_COOKIE['estadoCaixaDireita']))?$_COOKIE['estadoCaixaDireita']:'mostrar'; ?>
            <?php if ($estadoCookieDireita != 'ocultar') { ?>

                <div class="animacao-in mostra_oculta_logado tgo-fixed tgo-backgroundColorWhite tgo-borderBox tgo-borderLighter tgo-borderRadius4 tgo-xs-borderRadius0 tgo-topPercent tgo-right40 tgo-xs-initialPosition tgo-xs-bottom0 tgo-xs-maxSizeFullWidth tgo-xs-maxSizeFullHeight tgo-textAlignLeft uiScale maxw">

                    <div class="tgo-absolute box_cabecalho" style="left:0;">
                        <span class='classe_fechar' onclick='fecharDireita(this)'>x</span>
                    </div>
                    <div style="border-bottom: #757373 1px solid;background: #80808026;">
                        <span class="tipo_box_interna"><?php echo $direitaContCaixaNome; ?></span>
                        <br style="width: 100%;">
                    </div>
                    <div class="box_interna">

                        <?php if ($direitaTipo == 'conteudo') { ?>
                            <div>
                                <a href="<?php echo $esquerdaLink; ?>">
                                    <span class="float_titulo"><?php echo $direitaTitulo; ?></span>
                                </a>
                            </div>
                             <?php if($haDireitaImg){ ?>
                            <a href="<?php echo $direitaLink; ?>">
                                <img src="<?php echo $direitaImg; ?>" class="img_banner_float" alt="" />
                            </a>
                            <?php } ?>

                            <div class="ui-caption">
                                <?php //limitChars($direitaConteudo, 250); ?>
                                <?php echo htmlspecialchars_decode($direitaConteudo); ?>
                            </div>
                            <div class="ui-caption">
                                <!-- <a class="ds-link ds-link--styleSubtle" href="<?php echo $direitaLink; ?>" target="_blank">Ver mais &gt;&gt;</a> -->
                            </div>
                        <?php } elseif ($direitaTipo == 'ads') { ?>
                            <div class="ui-caption">
                                <?php echo htmlspecialchars_decode($direitaConteudo); ?>
                            </div>
                        <?php } else {
                        ?>
                            <div class="ui-caption">
                                <?php echo htmlspecialchars_decode($direitaConteudo); ?>
                            </div>
                        <?php
                    } ?>

                    </div>
                </div>
                <!-- Fim do box direita -->
                <?php } //Verifica cookie esquerda ?>
            <?php } ?>
        <?php } ?>

    </div>

    <script>
        //no arquivo require.js
    </script>
<?php } ?>

<!-- Código caixa_flutuante -->
<!-- Se a página não tiver jQuery, descomentar a linha abaixo -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
<div id="mostra_adboxes"></div>
<script>
    var UrlAds = "<?php echo URLSITE; ?>caixa_flutuante.php";
</script>
<script src="<?php echo URLSITE; ?>require.js"></script>
<!-- Código caixa_flutuante -->